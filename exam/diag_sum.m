function s = diag_sum(A)
[m,n] = size(A);
b = min(m,n);
i = (b+1)/2;
A = A([1:b],[1:b]);
E = flip(A);
if mod(b,2)==0
    s = sum(diag(A))+sum(diag(E));
else
    s = sum(diag(A))+sum(diag(E))-A(i,i);
end
end

